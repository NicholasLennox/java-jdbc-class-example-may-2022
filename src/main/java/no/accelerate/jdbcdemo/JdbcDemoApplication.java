package no.accelerate.jdbcdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JdbcDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(JdbcDemoApplication.class, args);
    }
}
